Este tutorial foi criado para o projeto 4 da disciplina de Aplicações Distribuídas da Universidade Federal de Goiás, afim de por em prática conceitos de P2P (Peer-to-Peer). Serão utilizados o Node.js e o VSCode como ferramentas.

Para as instruções e explicações Passo a Passo siga para a Wiki do Projeto.